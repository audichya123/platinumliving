# Platinum Living

## Table of Contents
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
  * [Setting up](#setting-up)
* [Running app locally](#running-app-locally)
* [Application development and testing process](#application-development-and-testing-process)
* [Troubleshooting](#troubleshooting)

## Getting Started
These instructions will help you set up a local development environment.

### Prerequisites
- Node `v8.12.0`
- MongoDB `v3.6.5`(current)
- Npm `6.4.1`
- Git ` 2.17.0`

### Installation
1. Clone repository:
```
$ git clone https://audichya123@bitbucket.org/audichya123/platinumliving.git
$ git fetch
$ git checkout development
```

2. Install Project Dependent Packages:
```
$ cd platinumliving
$ npm install
```


### Setting up

1. Set up environment variables:
Create a .env file in the project root directory.

  ```
  $ touch .env
  ```
env file should have following envioroment variables.
```
GOOGLE_APPLICATION_CREDENTIALS={Google Credentials.json file path}
```

## Running app locally

If you have followed all instructions up to this point, everything below should work fine. If not, make sure to:
- Install all the dependencies for your machine (npm packages)
- Prepare your environment variables.
- Prepare your database according to the database section.

1. Run the `npm` server in a separate terminal tab

Check node version on your computer:

```
$ node -v
```
Start the node server:
```
npm start
```


## Troubleshooting
1. Check weather mongod is running before running the server
2. Make sure the default server port 8080 is not running in any other service
3. Make sure project log folder has read/write permission
4. Make sure you have environment variables with correct values
5. Error: `npm install` causes errors
    - Make sure you are using correct node version:
    - Delete `node_modules/` folder: `rm -rf node_modules/`
    - Rerun `npm install`


### Issues

### Test suite

#### Test unit integration tests

#### Integration tests
